import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PartiListComponent } from './parti/partiList.component';
import { PartiCreateComponent } from "./parti/partiCreate.component";

const routes: Routes = [
    { path: 'parti', component: PartiListComponent },
    { path: 'parti/create', component: PartiCreateComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CereriRoutingModule { }
