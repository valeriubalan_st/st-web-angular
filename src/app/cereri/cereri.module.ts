import { NgModule } from '@angular/core';
import { CereriRoutingModule } from './cereri-routing.module';
import { SharedModule } from '../shared/shared.module';

import { PartiListComponent } from './parti/partiList.component';
import { PartiCreateComponent } from './parti/partiCreate.component';


@NgModule({
    imports: [
        CereriRoutingModule,
        SharedModule
    ],
    declarations: [
        PartiListComponent,
        PartiCreateComponent
    ]
})
export class CereriModule {
    constructor() {
    }
}
