import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//for securizations requests 
import { AuthHttp } from 'angular2-jwt';
//for url
import { Config } from './../../config';
//for form
import { ControlBase } from '../../shared/forms/control-base';
import { ControlTextbox } from '../../shared/forms/control-textbox';
//actions service for reseive data
import { ActionsService } from "../../services/actions.service";
//import model
import { PartieCreate } from '../../models/cereri/partie.create';

@Component({
    templateUrl: './partiCreate.component.html'
})
export class PartiCreateComponent implements OnInit {
    public errors: string[] = [];
    public controls: Array<ControlBase<any>>;

    constructor(private authHttp: AuthHttp,
        private router: Router,
        private actionService: ActionsService) {
    }
    /**
     * Implements initialization for this component
     */
    ngOnInit() {
        const controls: Array<ControlBase<any>> = [
            new ControlTextbox({
                key: 'username',
                label: 'Username',
                placeholder: 'Username',
                value: '',
                type: 'textbox',
                required: true,
                order: 1
            }),
            new ControlTextbox({
                key: 'firstname',
                label: 'Firstname',
                placeholder: 'Firstname',
                value: '',
                type: 'textbox',
                required: true,
                order: 2
            })
        ];

        this.controls = controls;
    }

    private onLanguageCodeChangedDataRecieved(item: string) {
        //some action
    }
    public create(model: PartieCreate): void {
        this.actionService.Create(model, "")
            .subscribe(
            () => {
                console.log("");
            },
            (error: any) => {
                console.log(error);
                this.errors = error;
            });
    };
}
