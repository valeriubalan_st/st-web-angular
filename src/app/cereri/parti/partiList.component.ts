﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthHttp } from 'angular2-jwt';
import { Config } from './../../config';
import { PartieModelList } from './../../models/cereri/parties';
import { Permissions } from "./../../models/permissions/permissions";

@Component({
    templateUrl: './partiList.component.html'
})
export class PartiListComponent implements OnInit {
    private PageTitle = "Title of page";
    public Tip = "list";
    public tableCols: Array<string>;
    public tableRows: any;
    public tableKeys: any;
    public partieModel: any;
    public currentPage = 1;
    public itemsperpage = 10;
    public totalitems = 0;
    private permissions: Permissions;
    private titleTable = "Parties";
    public allowPagination: boolean = true;


    constructor(private authHttp: AuthHttp, private router: Router) {
      
        this.settPage(1);
    }
    ngOnInit() {

    }
    public settPage(event: any): void {
        if (event.pageNo != null) this.currentPage = event.pageNo;
        this.authHttp.get("http://192.168.1.66:8989/api/ReportComplaintParties/getpage?page="
            + this.currentPage + "&&pageSize=" + 10)
            .subscribe(
            (res: any) => {
                this.tableCols = [
                    "Id-ul",
                    "ReportComplaintId-ul",
                    "PersonLastName",
                    "PersonFirstName",
                    "PersonIdnp"
                ];
                this.partieModel = res.json() as PartieModelList;
                this.tableRows = this.partieModel.result.result;
                this.tableKeys = Object.keys(this.partieModel.result.result[0]);

                //example custom columns from json
                /*this.tableKeys = [
                    "Id",
                    "PersonLastName"
                ];*/
                this.totalitems = this.partieModel.result.total_count;
                this.itemsperpage = this.partieModel.result.page_size;
            },
            (error: any) => {
                console.log(error);
            });
    };
}
