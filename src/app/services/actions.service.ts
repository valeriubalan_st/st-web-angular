import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { AuthHttp } from 'angular2-jwt';

import { Config } from '../config';
import { Observable } from 'rxjs/Observable';

/**
 * Service for Actions Create , update, delete
 */
@Injectable() export class ActionsService {
    public targetApi = Config.dataServer;
    /**
     * constructor
     * @param authHttp 
     * request with securization
     */
    constructor(private authHttp: AuthHttp) { }
    /**
     * Create method for forms
     * @param model 
     * model from form to save in database
     * @param target 
     * For identifyng url of api
     */
    public List():Observable<any>{
        return this.authHttp.get("http://localhost:8181/api/identity/GetAll")
         .map((res: Response) => {
                const body: any = res.json();
                //some actions
            }).catch((error: any) => {
                return Observable.throw(error);
            });
    }
    public Create(model: any, target: string): Observable<any> {
        let body = JSON.stringify(model);

        return this.authHttp.post(this.targetApi + target, body)
            .map((res: Response) => {
                const body: any = res.json();
                //some actions
            }).catch((error: any) => {
                return Observable.throw(error);
            });
    }
    /**
 * Edit method for forms
 * @param model 
 * model from form to update in database
 * @param target 
 * For identifyng url of api
 */
    public Edit(model: any, target: string): Observable<any> {
        let body = JSON.stringify(model);

        return this.authHttp.post(this.targetApi + target, body)
            .map((res: Response) => {
                const body: any = res.json();
                //some actions
            }).catch((error: any) => {
                return Observable.throw(error);
            });
    }
    /**
     * Delete method for forms
     * @param id 
     * Guid parameter for identifyng data.
     * @param target 
     * For identifyng url of api
     */
    public Delete(id: string, target: string): Observable<any> {
        return this.authHttp.delete(this.targetApi + target, id)
            .map((res: Response) => {
                const body: any = res.json();
                //some actions
            }).catch((error: any) => {
                return Observable.throw(error);
            });
    }
}