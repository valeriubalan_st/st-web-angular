export interface PartieModelList {
    is_error: boolean;
    is_success: boolean;
    error_keys: any;
    result: result;
}
interface result {
    total_count: number;
    page: number;
    page_size: number;
    total_pages: number;
    result: results;
}
interface results
{
    Id: any;
    PersonFirstName: string;
    PersonLastName: string;
    PersonIdnp: any;
    ComplaintId: any;
    ComplaintTitle: string;
}