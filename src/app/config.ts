/**
 * Configuration data for server access
 */
export class Config {
    //Configuration Open Id
    //http://192.168.1.70:5086/.well-known/openid-configuration
    //public static readonly dataServer: string = "http://localhost:8181";
    public static readonly dataServer: string = "http://192.168.1.66:5566"; //link of api

    public static readonly TOKEN_ENDPOINT: string = Config.dataServer + "/connect/token";

    public static readonly USERINFO_ENDPOINT: string = Config.dataServer + "/connect/userinfo";

    public static readonly GRANT_TYPE: string = "password";

    public static readonly REVOCATION_ENDPOINT: string = Config.dataServer + "/connect/revocation";

    public static readonly SCOPE: string = "PIGDWebAPI offline_access openid profile email";

    public static readonly CLIENT_ID: string = "PIGD Angular SPA";
}
