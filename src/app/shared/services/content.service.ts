﻿import { Injectable } from '@angular/core';

import { DataService } from './data.service';
declare var require: any;

@Injectable()
export class ContentService {

    constructor(public dataService: DataService) { }

    public get(lang?: string): any {
        return this.dataService.get("./Translations/" + (lang || 'en') + ".json");
    }
    public put(key?: string, value?: string): any {
        const fs = require('fs');
        const dir = './Translations';

        fs.readdir(dir, (err, files) => {
            alert(files.length);
        });
       
        return "Ok";
    }
}
