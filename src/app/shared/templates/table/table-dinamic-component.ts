import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { SharedModule } from './../../shared.module';
import { Permissions } from "./../../../models/permissions/permissions";
import { Router } from "@angular/router";

@Component({
    selector: 'table-dinamic',
    templateUrl: './table-dinamic-component.html'
})
export class DynamicTableComponent implements OnInit {
    public currentPage: number = 1;
    public maxSize: number = 1;
    public enumerate: number;
    @Input() public tableRows: any = [];
    @Input() public tableKeys: any = []; // data key from json
    @Input() public tableColumns: any = []; // custom column
    @Input() public tableTitle = "Default Title"; // default title for table
    @Input() public totalItems = 0; //total items from server data json
    @Input() public permissions: Permissions;
    @Input() public itemsperpage = 10;
    @Input() public allowPagination = false;
    @Output("settPage") settPage = new EventEmitter();
    /**
     * constructor
     */
    constructor(private router: Router) {
    }
    /**
     * On constructor init data
     */
    public ngOnInit() {
        this.maxSize = this.itemsperpage;
        this.enumerate = this.currentPage * this.itemsperpage;
    }
    /**
     * Set page for table
     * @param pageNo 
     * number for sett page
     */
    public setPage(pageNo: number): void {
        this.currentPage = pageNo;
    };
    /**
     * event on page change for extern utilization
     * @param event 
     * data for extern utilization
     */
    public pageChanged(event: any): void {
        this.enumerate = this.currentPage * this.itemsperpage;
        this.settPage.emit({
            pageNo: this.currentPage
        });
    };
    /**
     * redirect for any modules to create
     */
    public redirect(): void {
        this.router.navigate([this.router.url + '/create']);
    }
}