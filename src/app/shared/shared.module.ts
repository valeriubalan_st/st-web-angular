import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
//for table
import { DynamicTableComponent } from './templates/table/table-dinamic-component';
//for pages title
import { DynamicTitleComponent } from "./templates/titlePage";
//for pagination
import { PaginationDirective } from './pagination.component';
//for forms
import { DynamicFormComponent } from './forms/dynamic-form.component';
import { DynamicFormControlComponent } from './forms/dynamic-form-control.component';
import { ErrorMessageComponent } from './forms/error-message.component';
import { ErrorSummaryComponent } from './forms/error-summary.component';
import { FormControlService } from './forms/form-control.service';
//for files upload
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploadComponent } from "./files/file-upload";
//for translatiuon
import { TranslateModule } from '@ngx-translate/core';
import { ContentService } from './services/content.service';
import { DataService } from './services/data.service';

const sharedModules: any[] = [
    HttpModule,
    JsonpModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FileUploadModule,
    TranslateModule
];

@NgModule({
    imports: sharedModules,

    exports: [
        sharedModules,
        DynamicTableComponent,
        DynamicTitleComponent,
        // templates for forms
        DynamicFormComponent,
        DynamicFormControlComponent,
        ErrorSummaryComponent,
        ErrorMessageComponent,
        FileUploadComponent
    ],
    declarations: [
        DynamicTableComponent,
        DynamicTitleComponent,
        PaginationDirective,
        DynamicFormComponent,
        DynamicFormControlComponent,
        ErrorMessageComponent,
        ErrorSummaryComponent,
        FileUploadComponent
    ]
})

export class SharedModule {
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                FormControlService,
                ContentService,
                DataService
            ]
        };
    }
}
