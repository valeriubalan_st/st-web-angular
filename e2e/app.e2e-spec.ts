import { St.Test.ApiPage } from './app.po';

describe('st.test.api App', () => {
  let page: St.Test.ApiPage;

  beforeEach(() => {
    page = new St.Test.ApiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
